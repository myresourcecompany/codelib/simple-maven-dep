package com.example.dep;

/**
 * Hello name!
 *
 */
public class Dep
{
    private Dep() {
        throw new IllegalStateException("Utility class");
      }
    
    public static String hello( String name )
    {
        return "Hello " + name + "!";
    }
}
